const {ccclass, property} = cc._decorator;

@ccclass
export default class Camera extends cc.Component {

    @property(Number)
    MapW: number = 1920;

    @property(Number)
    MapH: number = 1280;

    private PlayreRef : cc.Node;
    private CameraW : number;
    private CameraH : number;
    

    // onLoad () {}

    start () {
        this.PlayreRef=cc.find("main/obj_head/Player");
        this.CameraW=480;//可調整
        this.CameraH=320;//可調整
    }

    update (dt) {
        var playerx : number;
        var playery : number;
        playerx=this.PlayreRef.position.x;
        playery=this.PlayreRef.position.y;
        if(playerx<this.CameraW/2){
            this.node.x=this.CameraW/2;
        }
        else if(playerx<this.MapW-this.CameraW/2){
            this.node.x=playerx;
        }
        else{
            this.node.x=this.MapW-this.CameraW/2;
        }
        if(playery<this.CameraH/2){
            this.node.y=this.CameraH/2;
        }
        else if(playery<this.MapH-this.CameraH/2){
            this.node.y=playery;
        }
        else{
            this.node.y=this.MapH-this.CameraH/2;
        }

        //debug
      


    }
}
