// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Button)
    sign_in_btn: cc.Button = null;
    @property(cc.Button)
    sign_up: cc.Button = null;
    @property(cc.Prefab)
    sign_up_input:cc.Prefab = null;

    

    // LIFE-CYCLE CALLBACKS:

     onLoad () {
        this.sign_up.node.on('click',this.generate_sign_up_input, this);
        this.sign_in_btn.node.on('click',this.generate_sign_in_input, this);

     }

    //start () {
     generate_sign_up_input(){
         //console.log("put the input")
         cc.director.loadScene("input_sign_up");

     }
     generate_sign_in_input(){
        cc.director.loadScene("input_sign_in");
     }
    //}

     //update (dt) {}
}
