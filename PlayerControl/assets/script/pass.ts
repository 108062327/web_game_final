// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    private changescene(event,data){
        if(data=='menu'){
            cc.director.loadScene("level_select");
        }
        else if(data=='next'){
            cc.director.loadScene(""/*next level name*/);
        }
    }
    private callback1(){
        this.node.getChildByName("score").runAction(cc.spawn(cc.moveBy(1.5,cc.v2(-150,0)),cc.fadeIn(1.5)));
    }
    private callback2(){
        this.node.getChildByName("menu").runAction(cc.spawn(cc.moveBy(1,cc.v2(0,200)),cc.fadeIn(1)));
        //this.node.getChildByName("next").runAction(cc.spawn(cc.moveBy(1,cc.v2(0,200)),cc.fadeIn(1)));
    }
    start () {
        this.node.getChildByName("level").runAction(cc.spawn(cc.moveBy(1.5,cc.v2(0,-50)),cc.fadeIn(1.5)));
        

        //this.scheduleOnce(this.callback1,1.5);
        this.scheduleOnce(this.callback2,2.5);
        
    }

    // update (dt) {}
}
