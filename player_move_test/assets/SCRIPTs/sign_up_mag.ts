// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.EditBox)
    user_name: cc.EditBox = null;
    @property(cc.EditBox)
    password: cc.EditBox = null;
    @property(cc.Button)
    send_btn: cc.Button = null
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.send_btn.node.on('click',this.check_user_file, this);
    }
    check_user_file(){
      var name = this.user_name.string;
      var pass = this.password.string;
      firebase.auth().createUserWithEmailAndPassword(name, pass).then((result) => {
        cc.director.loadScene("game_start");
        

    }).catch((err) => {
        console.log( err);
    });

      /*
        var gamers_ref = firebase.database().ref().child("game_users");
        var name = this.user_name.string;
        var pass = this.password.string;

        gamers_ref.orderByChild("username").equalTo(name)
        .once("value",function(snapshot) {
            if (snapshot.exists()) {
              var data = snapshot.val();
             
              if(data.password == pass){
                cc.director.loadScene("play_scene");
              }
              else{
                  alert("wrong password")
              }
            } else {
              alert("no such user");

              cc.director.loadScene("sign_in_scene");

            }
            ;
        });
      */ 
        

    }
    /*update (dt) {
        

    }*/
    
}
