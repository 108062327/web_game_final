// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase:any;
import data from './gamedata'
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        let userdata=cc.find('gamedata').getComponent(data);
        this.node.getChildByName("username").getComponent(cc.RichText).string='<color=#000000>'+userdata.name+'</color>';
        this.node.getChildByName("topic_left").runAction(cc.moveBy(1.5,cc.v2(100,0)));
        this.node.getChildByName("topic_right").runAction(cc.moveBy(1.5,cc.v2(-100,0)));
    }
/* button event*/
    private changescene(event,data){
        if(data=="1"){
            cc.director.loadScene("game") ;
        }
        else if(data=="2"){
            cc.director.loadScene(""/* level2 scene name*/ );
        }
        else if(data=="3"){
            cc.director.loadScene(""/* level3 scene name*/ );
        }
        else{
            firebase.auth().signOut().then(()=>{
                cc.director.loadScene("game_start");
            }).catch((err)=>{
                alert("log out failed.")
            })
        }
    }
    // update (dt) {}
}
