const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    public StartX : number;
    public StartY : number;

    private aDown : boolean;//left
    private dDown : boolean;//right
    private wDown : boolean;//jump
    private hDown : boolean;//sprint
    private jDown : boolean;//attract self to nearest Point(命名：相同的字串+照順序不跳號的數字)
    private isClimbing : boolean;
    private isFalling : boolean;
    private isJumping : boolean;
    private isGJumping : boolean;
    private isWin : boolean;

    private isSprinting : boolean;
    private isRespawning : boolean;
    private isHooking : boolean;
    private isDead : boolean;
    private onGround : boolean;
    private canDoubleJump;//二段跳的判斷
    private climbDirection : number;
    private freeHorizontal : boolean;//在空中可以自由左右移動
    private freeHorizontalCounter : number;//有一段時間不能左右移動
    //private previousPoint : number;//上一個使用過的Point的編號
    private accelerateCounter : number;

    private rigidBody : cc.RigidBody;
    private animation : cc.Animation;
    private drawing   : cc.Graphics;
    onLoad () {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity=new cc.Vec2(0,-480);

        this.drawing = this.node.addComponent(cc.Graphics);
        this.drawing.lineWidth = 6;

    }

    start () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.StartX=this.node.x;
        this.StartY=this.node.y;
        this.aDown=false; this.dDown=false; this.wDown=false; this.hDown=false; this.jDown=false;
        this.isClimbing=false; this.isJumping=false; this.isSprinting=false; this.isDead=false;
        this.isRespawning = false;
        this.onGround=false;
        this.canDoubleJump=false;
        this.isWin = false;
        this.isFalling=true;//初始位置離地面有一點點距離
        this.climbDirection=0;
        this.freeHorizontal=true;
        this.freeHorizontalCounter=0;
        this.accelerateCounter=0;
        this.isGJumping = false;
        this.rigidBody=this.getComponent(cc.RigidBody);
        this.animation=this.getComponent(cc.Animation);
    }

    update (dt) {

        if(this.isWin){
            if(!this.animation.getAnimationState("PlayerWin").isPlaying){
                this.animation.play("PlayerWin");
            }
        }
        else if(this.node.y<-64){
            this.isDead=true;
        }
        else if(this.isRespawning){
            //cc.log(this.node.x+","+this.node.y);

            return;
        }
        else if(this.isDead){
            cc.log("player dead!");
            //set conditions to prevent bugs
            this.isClimbing=false; this.isJumping=false; this.isSprinting=false; 
            this.isRespawning = false;
            this.onGround=false;
            this.canDoubleJump=false;
            this.isFalling=false;
            this.climbDirection=0;
            this.freeHorizontal=true;
            this.freeHorizontalCounter=0;
            this.accelerateCounter=0;
            this.rigidBody.linearVelocity=new cc.Vec2(0,0);
            if(!this.animation.getAnimationState("PlayerDead").isPlaying){
                this.animation.play("PlayerDead");
            }
            return;
        }
        else if(this.wDown){
            this.Jump();
        }
        
        this.PlayerMovement(dt);
        this.PlayerAnimation();
        this.CounterUpdate();
    }

    private Jump(){
        var jumpScale=180;//可調整
        var diagonalJumpScale=150;
        if(this.onGround){
            this.rigidBody.linearVelocity=new cc.Vec2(this.rigidBody.linearVelocity.x,jumpScale);
            this.isJumping=true;
            this.isGJumping = true;
            //this.canDoubleJump=true;
            this.onGround = false;//try
            cc.log('jump');
        }
        else if(this.isClimbing){
            cc.log("climb jump!");
            this.rigidBody.linearVelocity=new cc.Vec2(-diagonalJumpScale*this.climbDirection,jumpScale);
            this.isJumping=true;
            this.isClimbing = false;
            this.canDoubleJump=false;
            this.isFalling=false;
            this.freeHorizontal=false;
            this.freeHorizontalCounter=50;//可調整
        }
        else if(this.canDoubleJump&&!this.isGJumping){
            this.rigidBody.linearVelocity=new cc.Vec2(this.rigidBody.linearVelocity.x,jumpScale);
            this.isJumping=true;
            this.isGJumping = false;
            this.canDoubleJump=false;
            this.isFalling=false;
            cc.log("d jump!");
        }

        
        
    }

    private CounterUpdate(){
        if(this.freeHorizontal){
            this.freeHorizontalCounter=0;
        }
        else{
            this.freeHorizontalCounter-=1;
            if(this.freeHorizontalCounter<=0){
                this.freeHorizontalCounter=0;
                this.freeHorizontal=true;
            }
        }
        if(this.accelerateCounter>0){this.accelerateCounter-=1;}
    }

    private PlayerMovement(dt){
        var playerSpeedX : number;
        var speedScale : number;
        var accelerateScale : number;
        var maxDist : number;
        var minDist : number;
        speedScale=75;//可調整
        accelerateScale=10;//可調整
        maxDist=100;//可調整
        minDist = 20;

        if(this.jDown&&this.accelerateCounter<=0){
            //find nearest point
            cc.log("find point");
            var dist : number = -1;
            var pointCounter : number=1;
            var pointRef : cc.Node;
            var nearestPoint : cc.Node;
            var pointCord : cc.Vec2 = new cc.Vec2(0,0);
            var temp : number;
          
            while(1){
                pointRef=cc.find("main/obj_head/Point"+pointCounter);//命名注意
                if(pointRef==null){break;}
                cc.log("point "+pointCounter+" is found");
                pointCounter+=1;
                if(dist==-1){
                    dist=(pointRef.x-this.node.x)*(pointRef.x-this.node.x)+(pointRef.y-this.node.y)*(pointRef.y-this.node.y);
                    nearestPoint=pointRef;
                }
                else{
                    temp=(pointRef.x-this.node.x)*(pointRef.x-this.node.x)+(pointRef.y-this.node.y)*(pointRef.y-this.node.y);
                    if(temp<dist){
                        dist=temp;
                        nearestPoint=pointRef;
                    }
                }
            }
            pointCounter-=1;
            pointRef=nearestPoint;
            if(pointRef!=null&&dist<=maxDist*maxDist&& dist>= minDist*minDist){
                this.freeHorizontal=true;
                this.freeHorizontalCounter=0;//可調整
                this.accelerateCounter=0;//可調整
                pointCord.x=(pointRef.x-this.node.x);
                pointCord.y=(pointRef.y-this.node.y);
                cc.log(pointCord);
                pointCord.normalizeSelf();
                cc.log(pointCord);
                pointCord.x*=accelerateScale;
                pointCord.y*=accelerateScale;
                pointCord.x =pointCord.x + this.rigidBody.linearVelocity.x;
                pointCord.y =pointCord.y + this.rigidBody.linearVelocity.y;

                cc.log(pointCord);
                this.rigidBody.linearVelocity= pointCord;

                pointRef.color = cc.color(0,0,255,255);//change color if selected
                this.isHooking = true;
                //set a timer to turn it back
                setTimeout(function(){
                    pointRef.color = cc.color(255,0,0,255);
                },0.1);
                /* // tried to draw a line but failed
                this.drawing.lineTo(this.node.x,this.node.y);
                this.drawing.moveTo(pointRef.x,pointRef.y);
                this.drawing.stroke();*/
            }
            else{
                cc.log("point too far");
            }
        }
        else {this.isHooking = false;}
        if(this.freeHorizontal){
            if(this.hDown&&this.onGround){
                this.isSprinting=true;
                //this.isWalking = false;//if isSprinting not walking
            }
            if(!this.hDown){
                this.isSprinting=false;
                //this.isWalking = true;//if is walking not isSprinting

            }
            if((this.aDown&&this.dDown)||(!this.aDown&&!this.dDown)){//pressing or loosing both,stop moving
                playerSpeedX=0;
                this.isSprinting=false;
                //this.isWalking = false;
            }
            else if(this.aDown){
                this.node.scaleX=-1;
                if(this.isSprinting){
                    playerSpeedX=-2*speedScale;
                }
                else{
                    playerSpeedX=-speedScale;
                }
            }
            else if(this.dDown){
                this.node.scaleX=1;
                if(this.isSprinting){
                    playerSpeedX=2*speedScale;
                }
                else{
                    playerSpeedX=speedScale;
                }
            }
            else{
                playerSpeedX=0;
            }
            this.rigidBody.linearVelocity=new cc.Vec2(playerSpeedX,this.rigidBody.linearVelocity.y);
        }
        if(this.rigidBody.linearVelocity.y<0){
            this.isJumping=false;
            this.isFalling=true;
        }   

        if(this.isGJumping&&!this.wDown ){//must w up to double jump
            this.canDoubleJump = true;
            this.isGJumping = false;
            cc.log("can d jump");
        }
    }

    private PlayerAnimation(){
        var speedScale : number=75;//可調整
        var type : number;
        type=this.rigidBody.linearVelocity.x/speedScale;
        if(type<0){type*=-1;}

        if (this.isHooking){
            if(!this.animation.getAnimationState("PlayerHook").isPlaying)
                this.animation.play("PlayerHook");
        }
        else if(this.onGround){
            if(type==2){
                if(!this.animation.getAnimationState("PlayerSprint").isPlaying){
                    this.animation.play("PlayerSprint");
                }
            }
            else if(type==1){
                if(!this.animation.getAnimationState("PlayerWalk").isPlaying){
                    this.animation.play("PlayerWalk");
                }
            }
            else{
                if(!this.animation.getAnimationState("PlayerIdle").isPlaying){
                    this.animation.play("PlayerIdle");
                }
            }
        }
        else if(this.isJumping){
            if(!this.animation.getAnimationState("PlayerJump").isPlaying){
                this.animation.play("PlayerJump");
            }
        }
        else if(this.isFalling){
            if(!this.animation.getAnimationState("PlayerFall").isPlaying){
                this.animation.play("PlayerFall");
            }
        }
    }

    onBeginContact(contact,self,other){
        if(other.tag==1){
            cc.log(contact.getWorldManifold().normal.x+','+contact.getWorldManifold().normal.y);
            if(contact.getWorldManifold().normal.y<0){
                this.onGround=true;
                this.isFalling=false;
                this.isClimbing=false;
                this.isJumping=false;
                this.canDoubleJump=true;
                this.freeHorizontal=true;
                cc.log('hit ground');
            }
            if(contact.getWorldManifold().normal.x!=0&&!this.onGround){
                this.climbDirection=contact.getWorldManifold().normal.x;
                this.isClimbing=true;
                this.onGround=false;
                this.isFalling=false;
                this.isJumping=false;
                this.canDoubleJump=true;
                this.freeHorizontal=true;
                cc.log("is climbing");
                setTimeout(function(){
                    this.isClimbing=false;
                    cc.log("climb let go");
                },0.1);
            }
        }
        else if(other.tag == 2){//danger
            this.isDead = true;
        }
        else if(other.tag == 3){//win
            this.isWin = true;
        }
    }

    onEndContact(contact,self,other){
        if(other.tag==1){
            if(contact.getWorldManifold().normal.y==-1){
                this.onGround=false;
                cc.log('leave ground');
            }
            else if(contact.getWorldManifold().normal.x!=0&&!this.onGround){
                this.climbDirection=0;
                this.isClimbing=false;
            }
        }
    }

    onKeyDown(event){
        if(event.keyCode==cc.macro.KEY.a){
            this.dDown=false;
            this.aDown=true;
        }
        if(event.keyCode==cc.macro.KEY.d){
            this.aDown=false;
            this.dDown=true;
        }
        if(event.keyCode==cc.macro.KEY.w){
            this.wDown=true;
        }
        if(event.keyCode==cc.macro.KEY.h){
            this.hDown=true;
        }
        if(event.keyCode==cc.macro.KEY.j){
            this.jDown=true;
        }
    }

    onKeyUp(event){
        if(event.keyCode==cc.macro.KEY.a){
            this.aDown=false;
        }
        if(event.keyCode==cc.macro.KEY.d){
            this.dDown=false;
        }
        if(event.keyCode==cc.macro.KEY.w){
            this.wDown=false;
        }
        if(event.keyCode==cc.macro.KEY.h){
            this.hDown=false;
        }
        if(event.keyCode==cc.macro.KEY.j){
            this.jDown=false;
        }
    }


    Dead(){
        //cc.log("player dead!");
            //reg a callback for dead anmi
           // var dead_callback=this.animation.on("finished",function(){
        cc.log("dead complete");
        
        this.isDead = false;
        this.isRespawning = true;

        cc.log("start respawn!");
        this.node.x=this.StartX;
        this.node.y=this.StartY;
        this.animation.play("PlayerRe");
        /*cc.log("start respawn done!");

        this.isRespawning = false;*/

    }
    YO_Respawn(){
        cc.log("respowned!");
            //reg a callback for respawn anmi
        this.isDead = false;
        this.isRespawning = false;    
    }
    GameWin(){
        
    }
}
